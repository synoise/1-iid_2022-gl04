# ---------------------
#    Labaratorium 3
# ---------------------
from collections.abc import Iterable

# Exersise 1
list1 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
list1[2][2].append(7000);
print(list1)


# Exersise 2

def powerToInArr(arr, p):
    newArr = arr
    for el in newArr:
        if isinstance(el, list):
            powerToInArr(el, p)
        else:
            newArr[newArr.index(el)] = el ** p
            # Why "el **= p" does not work?
    return newArr


print(
    powerToInArr(list1, 2)
)

# Exersise 3

list1 = ["M", "n", "im", "Pio"]
list2 = ["am", "a", "ie", "tr"]

def appendArrs (arr1, arr2):
    newArr = arr1
    for i in range(len(newArr)):
        if isinstance(arr1[i], int) or isinstance(arr1[i], str):
            newArr[i] += arr2[i]
    return newArr

print(appendArrs (list1, list2)