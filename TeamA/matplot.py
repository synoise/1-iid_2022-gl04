import matplotlib.pyplot as plt

axesX=input("x").split(" ")
axesY=input("y").split(" ")
for i in range(len(axesX)):
    axesX[i] = int(axesX[i])
    axesY[i] = int(axesY[i])
plt.plot(axesX, axesY, 'ro')
plt.axis([0 if min(axesX)>0 else min(axesX)-1, 0 if max(axesX)<0 else max(axesX)+1, 0 if min(axesY)>0 else min(axesY)-1, 0 if max(axesY)<0 else max(axesY)+1])
plt.show()