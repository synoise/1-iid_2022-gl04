import random

def zad9():
    for i in range(11):
        print(i, end=" ")


def zad10():
    for i in range(1,11):
        txt = ""
        for j in range(1,11):
            txt = txt + str(i*j) + "  "
        print(txt)


def zad11(leng):
    def fill(n):
        r = random.randint(0,n)
        return r

    macierz = []
    for i in range(leng):
        tab = []
        for j in range(leng):
            tab.append(fill(7))
        macierz.append(tab)
    print(macierz)

    def search(n):
        count = 0
        for i in range(len(macierz)):
            for j in range(len(macierz[i])):
                if n == macierz[i][j]:
                    count += 1
        print(count)
    search(3)


zad11(4)