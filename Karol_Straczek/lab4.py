import random


def zad4():
    lista = []
    names = ["Karol", "Maciej", "Jakub", "Piotr", "Krzysztof"]
    surnames = ["Kowalski", "Nowak", "Suchodolski", "Górny", "Wawrzynaik"]

    def getname():
        return random.choice(names)

    def getsurname():
        return random.choice(surnames)

    def fill(size):
        slownik = {}
        for i in range(size):
            slownik = {'names': getname(), 'surnames': getsurname(), 'phone': random.randint(5000000, 8000000),
                       'age': random.randint(18, 70)}
            lista.append(slownik)

        return lista

    def show():
        for i in lista:
            print(i)

    def search(par):
        namelist = []
        for i in lista:
            if par in i.values() or int(par) in i.values():
                namelist.append(i)
        return namelist

    fill(4)
    show()
    search(38)


def zad1():
    n = 30

    for i in range(1, 30):
        for j in range(1, 30):
            for k in range(1, 30):
                if (i ** 2 + j ** 2) == k ** 2:
                    print(i, j, k)


def zad2():
    n = int(input("Podaj n = "))
    suma = 0
    for i in range(1, n + 1):
        if i % 2 == 0 or i == 1:
            suma += i
            print(suma, i)
        else:
            suma -= i
            print(suma, i)
    print(suma)


def zad3():
    lista = []

    def fill(n):
        for i in range(n):
            lista.append(random.randint(0, n))
        print(lista)

    def search():
        numb = lista[0]
        count = 0
        for i in lista:
            currcount = lista.count(i)
            if currcount > count:
                count = currcount
                numb = i
        return print(numb)

    fill(10)
    search()


def zad5():
    tab = []
    s = ""

    d = 0
    for i in range(1000, 9999):
        n = i
        for j in range(2):
            d = n % 100
            tab.append(d)
            n = n // 100
        tab.reverse()
        s = str(tab[0]) + str(tab[1])
        if int(s) == i:
            print(i, True)
        else:
            print(i, False)


def zad8(e):
    def silnia(n):
        return n * silnia(n - 1) if n > 1 else 1

    suma = 0
    for i in range(e):
        suma += 1 / silnia(i)
    print(suma)


def zad9():
    dane = {'item1': 45.50, 'item2': 35, 'item3': 41.30, 'item4': 55, 'item5': 24}
    lista = list(dane.values())
    tab = []

    # for i in dane.items():
    for i in range(3):
        max = lista[0]
        for j in lista:
            if max < j:
                max = j
                tab.append(max)
                lista.remove(max)

    print(lista)
    print(tab)


zad9()
