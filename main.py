# astring = True;
# print("single quotes are ' '")
# print(len(astring))
# print(astring.index("o"))
# print(astring.count("l"))
# print(astring[3:7])
# print(astring[:4])
# print(astring[3:])
# print(astring[3:7:1])
# print(not (astring))
# print(astring.upper())
# print(astring.lower())
# print(astring.startswith("Hello"))
# print(astring.endswith("asdfasdfasdf"))
# afewwords = astring.split(" ")

# va2 = "w" is not "w"
# print(va2)
# True
# 1 is 1
# True
# (1,) == (1,)
# True
# (1,) is (1,)
# False
# a = (1,)
# b = a
# a is b
# True

# x: int = 3`
# x = input()
# print(x)

# while x <= 100:  # dopóki warunek będzie prawdziwy, powtórz poniższe instrukcje
#     print(x)  # wypisz wartość zmiennej x
#     x = x + 1  # zwiększ wartość zmiennej x o 1

# import math
#
#
# def kwadratowa():
#     a: int = int(input("modal a"));
#     b: int = int(input("podaj b"));
#     c: int = int(input("podaj c"));
#
#     def obliczDelte(_a, _b, _c):
#         _delta = _b * _b - 4 * _a * _c
#         return _delta
#
#     def switch(delta):
#         if delta > 0:
#             x1 = (-b - math.sqrt(delta)) / (2 * a)
#             x2 = (-b + math.sqrt(delta)) / (2 * a)
#             print("x1=" + str(x1) + " x2=" + str(x2))
#         elif delta == 0:
#             x3 = -b / (2 * a)
#             print("x=" + x3)
#         elif delta < 0:
#             print("nara")
#
#     # switch(obliczDelte(a, b, c))
#
#
# # kwadratowa()
#
# # Napisz program, który oblicza pole prostokąta. Wartości boków a i b są typu float i należy je wprowadzić z klawiatury. Wynik działania programu należy wyprowadzić na ekran komputera.
#
# # Zadanie 1.1.
# # To jest komentarz.
#
# def poleProsto():
#     print("Program oblicza pole prostokąta.")
#     a = float(input("Podaj bok a = "))  # Czytanie z klawiatury liczby rzeczywistej a.
#     b = float(input("Podaj bok b = "))  # Czytanie z klawiatury liczby rzeczywistej
#     print()  # Wyświetlenie pustej linii.
#     pole = a * b  # Obliczanie pola prostokąta.
#     print("Dla a =", str(a), "i b =", str(b))  # Wyświetlenie zmiennych a i b.
#     print("pole prostokąta =", pole)  # Wyświetlenie zmiennej pole.
#
#
# # poleProsto()
# # Napisz program, który wczytuje imię, nazwisko, wiek oraz cenę chleba, a następnie te cztery zmienne drukuje na ekranie komputera.
#
# def chleb():
#     print("Podaj swoje imię.")
#     imię = input()
#     print("Podaj swoje nazwisko.")
#     nazwisko = input()
#     wiek = int(input("Ile masz lat? "))
#     cena = float(input("Ile płaciłeś za chleb? "))
#     print()
#     print("Oto wprowadzone przez Ciebie dane:")
#     print("Imię: ", imię, ".")
#     print("Nazwisko: ", nazwisko, ".")
#     print("Wiek:", wiek, "lata.")
#     print("Chleb kosztuje:", cena, "zł.")
#
#
# #chleb()
#
# # Napisz program, w który generuje 5 liczb pseudolosowych z przedziału od 1 do 100.
# import random  # Importujemy do programu moduł random.
#
# def pseudolosowe():
#     print("Liczby pseudolosowe: ")
#     print()
#     for i in range(5):
#         liczba = random.randint(1, 100)  # Generowanie liczby pseudolosowej. print(liczba)
#         print(liczba)
#
#
# # Zadanie jest następujące. Przy użyciu języka Python, należy znaleźć najmniejszą oraz największa liczbę na liście.
#
# def mniejszaWieksza():
#     lista = [1, 2, 3, 4, 345, 345, 543, 123, 4]
#
#     najmniejsza = None
#     najwieksza = None
#
#     for i in lista:
#
#         if najmniejsza == None or najmniejsza > i:
#             najmniejsza = i
#
#         if najwieksza == None or najwieksza < i:
#             najwieksza = i
#
#     print("najmniejsza liczba to:", najmniejsza)
#     print("największa liczba to:", najwieksza)


# Bardzo popularnym zadaniem, szlifującym nasz język, jest program do analizy ciągu znaków,
# czyli przykładowo zliczenia liczby słów czy też znaków.

# def mniejszaWieksza():
#     string = "ABC for the purpose of LetterCounting program"
#     print(string)
#     words = 1
#     letters = 0
#     hash_table = {}
#     for char in string:
#         char = char.lower()
#         if char == ' ':
#             words += 1
#         else:
#             letters += 1
#             if char in hash_table:
#                 hash_table[char] += 1
#             else:
#                 hash_table[char] = 1
#
# print("Words:", words, "Letters:", letters, "Freq:", hash_table)
#
# # 5 Napisz program w Pythonie konwertujący liczbę całkowitą na binarną, zachowaj wiodące zera
# x = 12
# print(format(x, '08b'))
# print(format(x, '010b'))

# 6 Napisz program który zliczy wyrazy w ciągu zanków oraz zliczy litery i ich częstotliwość występowania

# txt = "welcome to the jungle"
#
# x = txt.split(" ")
#
# print(len(x), x)


# import matplotlib.pyplot as plt
# x = [1 , 2 , 3 , 4 , 5]
# y = [1 , 4 , 9 , 16 , 25]
# y2 = [1 , 3 , 5 , 7 , 9]
# plt.plot (x , y )
# plt.plot (x , y2 )
# print(123)

#
# tab =[0,1,2,3,4,5,6,7,8,9,10]
# row = ""
# for j in tab:
#     for i in tab:
#         row = row + " " +str(tab[i]* tab[j] )
#     print(row)
#     row = ""


# tab =[0,1,2,3,4,5,6,7,8,9,10]
# row = ""
# for j in range(1, 11):
#     for i in range(1, 11):
#         row = row + " " + str(i * j)
#     print(row)
#     row = ""
#
#
# def char_frequency(str1):
#     dict = {}
#     for n in str1:
#         keys = dict.keys()
#         if n in keys:
#             dict[n] += 1
#         else:
#             dict[n] = 1
#     return dict
#
#
# print(char_frequency('google.com'))


# lista = [1, 2, 235, -45, 456, 234, -34, -233.44, 2]
#
# dict = {'imie': 'Piotr', "nazwisko": "Synoś"}
#
# print(dict["imie"])
#
# slownik = {"class":
#                {"student":
#                     {"name": "Mike",
#                      "marks":
#                          {"physics": 70, "history": 80}
#                      }
#                 }
#            }
#
# print(slownik["class"]["student"]["name"])



ZAKRES = 10
branchOfTree = ""
space = ""
belowe= "+"
for j in range(ZAKRES):
    space += " "
    belowe += "+"

for i in range(ZAKRES):
    branchOfTree = branchOfTree + "*"
    # space.split(1)
    if(ZAKRES == i+1):
        print(belowe)
    else:
        print(space[i:ZAKRES] + branchOfTree)
