import random

names = ["Dmytro", "Andrzej", "Tom", "Benedykt"]
surnames = ["Pelyp", "Pilsudski", "Mayer"]


def createList (listLength):
    list = []
    for i in range(listLength):
        name = random.choice(names)
        surname = random.choice(surnames)
        age = random.randint(18, 70)
        phoneNumber = random.randint(5000000, 8000000)
        list.append({'name': name, 'surname':surname, 'age':age, 'phoneNumber': phoneNumber})
    return list

def printLista (list):
    for i in list:
        print(i)

def search (list, par):
    newList = []
    for i in list:
        if par in i.values() or int(par) in i.values():
            newList.append(i)
    return newList

length = int(input("enter length: "))

list = createList(length)
printLista(list)
while 1:
    par = input("enter par: ")
    print(search(list, par))


def task3():
    arr = []

    def fillArr(n):
        for i in range(n):
            arr.append(random.randint(0, n))
        print(arr)

    def search():
        numb = arr[0]
        count = 0
        for i in arr:
            currcount = arr.count(i)
            if currcount > count:
                count = currcount
                numb = i
        return print(numb)

    fillArr(10)
    search()