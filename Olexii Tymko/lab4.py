import random
imiona = ["Oleksii", "Andrzej", "Tom", "Benedykt"]
nazwiska = ["Tymko", "Pilsudski", "Mayer"]


def createList (listLength):
    list = []
    for i in range(listLength):
        name = random.choice(imiona)
        surname = random.choice(nazwiska)
        age = random.randint(18, 70)
        phone = random.randint(5000000, 8000000)
        list.append({'name': name, 'surname':surname, 'age':age, 'phone': phone})
    return list

def printLista (list):
    for i in list:
        print(i)

def search (list, par):
    newList = []
    for i in list:
        if par in i.values() or int(par) in i.values():
            newList.append(i)
    return newList

length = int(input("length: "))

list = createList(length)
printLista(list)
while 1:
    par = input("pary: ")
    print(search(list, par))
