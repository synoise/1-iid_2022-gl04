import random

tab = []
wielokrotnosc = []


def funA(n):
    for i in range(0, n):
        tab.append(random.randint(0, 10))
    return tab


def funB(x):
    for j in x:
        if j == 0:
            continue
        elif j % 3 == 0 or j % 5 == 0:
            wielokrotnosc.append(j)

    return wielokrotnosc


def srednia(v):
    suma = 0
    srednia = 0
    for k in v:
        suma += k
    if len(v) != 0:
        srednia = suma / len(v)
    return srednia


def czy_fibbonacci(l):
    fibbonacci = [0, 1]
    for h in range(0, 10):
        fibbonacci.append(fibbonacci[h] + fibbonacci[h + 1])
    for t in fibbonacci:
        if l == t:
            print("Liczba", l, "należy do ciagu Fibonacciego.")


print("Zadanie A:", funA(5))
print("Zadanie B:", funB(tab))
print("Średnia dla tablicy z zadania A:", srednia(tab))
print("Średnia dla tablicy z zadania B:", srednia(wielokrotnosc))
czy_fibbonacci(15)

print("Zadanie E")
for y in tab:
    czy_fibbonacci(y)