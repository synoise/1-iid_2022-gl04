import random

names = ["Jakub", "Andrzej", "Tomasz", "Sebastian", "Karol"]
surnames = ["Kowalski", "Kot", "Małysz", "Kowal"]


def getname():
    return random.choice(names)


def getsurname():
    return random.choice(surnames)


def wypelnij(size):
    lista = []
    for i in range(size):
        slownik = {'names': getname(), 'surname': getsurname(), 'phone': random.randint(500000000, 800000000),
                   'age': random.randint(18, 50)}
        lista.append(slownik)
    return lista


def wypisz(lista):
    for j in lista:
        print(j)


def wyszukaj(par, lista):
    nameList = []
    for i in lista:
        if par in i.values() or int(par) in i.values():
            nameList.append(i)
    return nameList


# listaOsob = wypelnij(20)
# wypisz(listaOsob)
# print(wyszukaj(input("Podaj parametr: ") ,listaOsob))

# Zad2
def pitagoras(liczba):
    for i in range(3, liczba):
        for j in range(i, liczba):
            for k in range(3, liczba):
                if i ** 2 + j ** 2 == k ** 2:
                    print(i, j, k)


# pitagoras(30)

# Zad3
def zad3(sizeTab):
    tab = []
    liczba = 0
    for i in range(sizeTab):
        tab.append(random.randint(0, 10))
        tab.sort()
    for j in tab:
        if tab.count(j) > tab.count(j + 1):
            liczba = j
    print(liczba)
    return tab

print(zad3(5))

