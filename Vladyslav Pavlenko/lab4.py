
#===========================Task 1========================
# def dodacz(a):
#     for i in range(1, a+1):
#         if i % 2 == 0:
#             print(i - (i+1))
#         else:
#             print(i + (i+1))
#
# dodacz(10)


#========================Task 2=======================
# from turtle import *
# import random
#
# r = 63
# bokKwadrata = 2*r
#
# dots = []
#
# iloscPunktow = 100
# i = 0
#
# while i != iloscPunktow:
#     dots.append([random.uniform(0, bokKwadrata), random.uniform(0, bokKwadrata)])
#     i += 1
#
# circleCenter = [r, r];
#
# count = 0
# for i in range(len(dots)):
#     if (dots[i][0]**2 + dots[i][1] ** 2 <= r ** 2 ):
#         count += i
#         print(dots[i], dots[i][0]**2, dots[i][1] ** 2, r**2)
#
# print(count)
#
# def draw():
#     wn.setworldcoordinates(1, 1, bokKwadrata, bokKwadrata)
#     turtle.forward(bokKwadratra)
#     turtle.right(-90)
#     turtle.forward(bokKwadrata)
#     turtle.done()
#
#
# while 1:
#     draw()



import random  # Importujemy moduł random. import math # Importujemy moduł math.
import datetime  # Importujemy moduł datetime.
import math


def main():  # Definicja funkcji o nazwie main().
    start = datetime.datetime.now()  # Odczytujemy czas z systemuoperacyjnego.
    print("Aktualna data i czas systemu operacyjnego: ", end="")
    print(start, ".", sep="")
    print()
    liczba_punktow = 1000000  # Liczba punktów. licznik = 0
    print("Liczba punktów = ", liczba_punktow, ".", sep="")
    print("Proszę chwilę zaczekać...")
    print()
    licznik = 0
    for i in range(1, liczba_punktow, 1):
        x = random.uniform(0, 1)  # Generuje liczbę rzeczywistą z przedziału 0, 1.
        y = random.uniform(0, 1)  # Generuje liczbę rzeczywistą z przedziału 0, 1.
        if x * x + y * y <= 1:
            licznik = licznik + 1  # Sprawdza, czy punkty znajdują się w kole o pro-mieniu <= 1.
    pi = 4.0 * licznik / liczba_punktow  # Obliczona wartość pi. print("Stała pi = ", math.pi, ".", sep = "")
    print()
    print(f'Obliczona wartość pi = {pi:.8f}', ".", sep="")
    print()
    print("Różnica = math.pi - pi ", sep="", end="")
    print(f'= {abs(math.pi - pi):.4f}', ".", sep="")
    stop = datetime.datetime.now()  # Odczytujemy czas z systemu operacyjnego.
    print()
    print("Czas obliczeń: ", end="")
    print(stop - start, " sekund.", sep="")
    print("Zakończenie programu.")


    figure, axes = plt.subplots()
    Drawing_colored_circle = plt.Circle((0, 0), 2)

    axes.set_aspect(1)
    axes.add_artist(Drawing_colored_circle)
    plt.title('Colored Circle')
    plt.show()